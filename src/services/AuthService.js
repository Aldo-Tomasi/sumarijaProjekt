class AuthService {
  loginState = getLoginState
  getLoginState () {
    this.$auth.onAuthStateChanged((user) => {
      if (user) {
        // User is signed in.
        console.log('Index.vue: USER IS SIGNED IN')
        return true
      } else {
        console.log('Index.vue: NO USER IS SIGNED IN')
        return false
        // No user is signed in.
      }
    })
  }
}

const service = new AuthService()
export default service

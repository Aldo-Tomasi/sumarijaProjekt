import firebase from 'firebase/app'
// Add the Firebase services that you want to use
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/storage'

const firebaseConfig = {
  apiKey: 'AIzaSyAoOyo7wpMF7-U5OMJDhpe9aOcrHElS3kE',
  authDomain: 'sumarija-8246f.firebaseapp.com',
  databaseURL: 'https://sumarija-8246f.firebaseio.com',
  projectId: 'sumarija-8246f',
  storageBucket: 'sumarija-8246f.appspot.com',
  messagingSenderId: '387339458065',
  appId: '1:387339458065:web:65dfbd636b8467595fc3b0',
  measurementId: 'G-GY4XNQ98JH'
}

firebase.initializeApp(firebaseConfig)

export default ({ Vue }) => {
  Vue.prototype.$auth = firebase.auth()
  Vue.prototype.$db = firebase.firestore()
  Vue.prototype.$storage = firebase.storage()
}

import { Notify } from 'quasar'

Notify.setDefaults({
  position: 'center',
  timeout: 2500,
  textColor: 'white',
  actions: [{ icon: 'close', color: 'white' }]
})

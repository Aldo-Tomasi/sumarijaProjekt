const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: 'motrionice', component: () => import('pages/MotrionicePage.vue') },
      { path: 'opcine', component: () => import('pages/OpcinePage.vue') },
      { path: 'radnici', component: () => import('pages/RadniciPage.vue') },
      { path: 'uredjaji', component: () => import('pages/UredjajiPage.vue') },
      { path: 'dojave', component: () => import('pages/DojavnaPage.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
